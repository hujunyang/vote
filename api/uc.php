<?php

/**
 * 该通知实现 Discuz 论坛或者 UCenter 操作用户、应用设置后同步更新到微擎相关表和文件，仅实现单向通讯（Discuz --> 微擎）
 *
 * 相关软件测试版本：we7、Discuz_X3.2_SC_UTF8
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
error_reporting(0);

define('UC_CLIENT_VERSION', '1.6.0');
define('UC_CLIENT_RELEASE', '20110501');

define('API_DELETEUSER', 1);
define('API_RENAMEUSER', 1);
define('API_GETTAG', 1);
define('API_SYNLOGIN', 1);
define('API_SYNLOGOUT', 1);
define('API_UPDATEPW', 1);
define('API_UPDATEBADWORDS', 1);
define('API_UPDATEHOSTS', 1);
define('API_UPDATEAPPS', 1);
define('API_UPDATECLIENT', 1);
define('API_UPDATECREDIT', 1);
define('API_GETCREDIT', 1);
define('API_GETCREDITSETTINGS', 1);
define('API_UPDATECREDITSETTINGS', 1);
define('API_ADDFEED', 1);
define('API_RETURN_SUCCEED', '1');
define('API_RETURN_FAILED', '-1');
define('API_RETURN_FORBIDDEN', '1');

define('IN_SYS', true);
define('DISCUZ_ROOT', __DIR__ . '/');
require '../framework/bootstrap.inc.php';
$queryString = str_replace('?', '&', $_SERVER['QUERY_STRING']);
parse_str($queryString, $query);
load()->func('logging');
if (is_array($query)) {
    $sql = "SELECT `uc` FROM " . tablename('uni_settings') . " WHERE `uniacid`=:uniacid LIMIT 1";
    $setting = pdo_fetch($sql, array(':uniacid' => $query['uniacid']));
    if (!empty($setting) && !empty($setting['uc'])) {
        $uc = iunserializer($setting['uc']);
        if (!empty($uc) && $uc['status'] == '1') {
            define('UC_CONNECT', $uc['connect'] == 'mysql' ? 'mysql' : '');
            define('UC_DBHOST', $uc['dbhost']);
            define('UC_DBUSER', $uc['dbuser']);
            define('UC_DBPW', $uc['dbpw']);
            define('UC_DBNAME', $uc['dbname']);
            define('UC_DBCHARSET', $uc['dbcharset']);
            define('UC_DBTABLEPRE', $uc['dbtablepre']);
            define('UC_DBCONNECT', $uc['dbconnect']);

            define('UC_CHARSET', $uc['charset']);
            define('UC_KEY', $uc['key']);
            define('UC_API', $uc['api']);
            define('UC_APPID', $uc['appid']);
            define('UC_IP', $uc['ip']);

            $get = $post = array();
            parse_str(authcode($query['code'], 'DECODE', UC_KEY), $get);

            if (TIMESTAMP - $get['time'] > 3600) {
                exit('Authracation has expiried');
            }
            if (empty($get)) {
                exit('Invalid Request');
            }

            include_once IA_ROOT . '/framework/library/uc/lib/xml.class.php';
            $input = file_get_contents('php://input');
            $post = xml_unserialize($input);

            if ($get) {
                logging_run($get);
            }

            if ($post) {
                logging_run($post);
            }

            if (in_array($get['action'], array('test', 'deleteuser', 'renameuser', 'gettag', 'synlogin', 'synlogout', 'updatepw', 'updatebadwords', 'updatehosts', 'updateapps', 'updateclient', 'updatecredit', 'getcredit', 'getcreditsettings', 'updatecreditsettings', 'addfeed'))) {
                $note = new uc_note($query['uniacid']);
                die($note->$get['action']($get, $post));
            } else {
                exit(API_RETURN_FAILED);
            }
        }
    }
}

/**
 * 通讯接口类
 *
 * @see http://faq.comsenz.com/library/UCenter/api/api_index.htm
 */
class uc_note
{

    /**
     * 当前公众号 id
     *
     * @var integer
     */
    private $uniacid;

    /**
     * Discuz 表前缀
     *
     * @var string
     */
    private $discuzTablePrefix;

    /**
     * 论坛数据库连接
     *
     * @var $discuz medoo();
     */
    private $discuzDb;

    function __construct($uniacid)
    {
        $this->uniacid = (int) $uniacid;
        require __DIR__ . '/medoo.php';
        $this->discuzTablePrefix = str_replace(UC_DBTABLEPRE, 'ucenter_', '');
        $this->discuzDb = new medoo([
            'database_type' => 'mysql',
            'database_name' => UC_DBNAME,
            'server' => UC_DBHOST,
            'username' => UC_DBUSER,
            'password' => UC_DBPW,
            'charset' => UC_CHARSET,
            'prefix' => $this->discuzTablePrefix,
        ]);
    }

    /**
     * 根据 Ucenter uid 查找对应微擎 uid
     *
     * @param integer $ucenterUserId
     * @return integer
     */
    private function ucenterUid2WeUid($ucenterUserId)
    {
        return (int) pdo_fetchcolumn('SELECT uid FROM ' . tablename('mc_mapping_ucenter') . ' WHERE uniacid = :uniacid AND centeruid = :centeruid', array(':uniacid' => $this->uniacid, ':centeruid' => $ucenterUserId));
    }

    function _serialize($arr, $htmlon = 0)
    {
        return xml_serialize($arr, $htmlon);
    }

    function test($get, $post)
    {
        return API_RETURN_SUCCEED;
    }

    /**
     * 同步删除用户
     *
     * @global array $_W
     * @param array $get
     * @param array $post
     * @return integer
     */
    function deleteuser($get, $post)
    {
        global $_W;
        if (!API_DELETEUSER) {
            return API_RETURN_FORBIDDEN;
        }

        $success = true;
        $uids = str_replace("'", '', stripslashes($get['ids']));
        $uids = (!empty($uids)) ? array_unique(explode(',', $uids)) : array();
        foreach ($uids as $uid) {
            $success = pdo_delete('mc_mapping_ucenter', array('uniacid' => $this->uniacid, 'centeruid' => $uid));
            if (!$success) {
                break;
            }
        }

        return $success ? API_RETURN_SUCCEED : API_RETURN_FAILED;
    }

    /**
     * 同步用户名修改
     *
     * @global array $_G
     * @param array $get
     * @param array $post
     * @return integer
     */
    function renameuser($get, $post)
    {
        global $_W;

        if (!API_RENAMEUSER) {
            return API_RETURN_FORBIDDEN;
        }

        $success = true;
        $uid = $this->ucenterUid2WeUid($get['uid']);
        if ($uid) {
            $newUsername = $get['newusername'];
            $configs = array(
                'mc_members' => array('id' => 'uid', 'name' => 'nickname'),
            );
            foreach ($configs as $tableName => $config) {
                $result = pdo_update($tableName, array($config['name'] => $newUsername), array($config['id'] => $uid));
                if ($result === false) {
                    $success = false;
                    break;
                }
            }
        } else {
            $success = false;
        }

        return $success ? API_RETURN_SUCCEED : API_RETURN_FAILED;
    }

    function gettag($get, $post)
    {
        global $_G;
        if (!API_GETTAG) {
            return API_RETURN_FORBIDDEN;
        }
        return $this->_serialize(array($get['id'], array()), 1);
    }

    /**
     * 同步登录处理
     *
     * @global array $_G
     * @param array $get
     * @param array $post
     * @return mixed
     */
    function synlogin($get, $post)
    {
        global $_W;
        if (!API_SYNLOGIN) {
            return API_RETURN_FORBIDDEN;
        }
        load()->app('common');
        require IA_ROOT . '/app/common/bootstrap.app.inc.php';
        $_W['uniacid'] = $this->uniacid;
        $_W['openid'] = $this->uniacid;
        load()->model('mc');

        $ucenterUserId = intval($get['uid']);
        $user = array();
        $exist = pdo_fetch('SELECT * FROM ' . tablename('mc_mapping_ucenter') . ' WHERE `uniacid` = :uniacid AND `centeruid` = :centeruid', array(':uniacid' => $this->uniacid, 'centeruid' => $ucenterUserId));

        if (!empty($exist)) {
            $user['uid'] = $exist['uid'];

            return _mc_login($user) ? API_RETURN_SUCCEED : API_RETURN_FAILED;
        } else {
            $dzMember = $this->discuzDb->get('ucenter_members', array("[>]common_member_profile" => ["uid" => "uid"]), array('ucenter_members.username', 'ucenter_members.email', 'ucenter_members.password', 'ucenter_members.salt', 'common_member_profile.realname', 'common_member_profile.mobile'), array('ucenter_members.uid' => 1));
            $defaultGroupId = pdo_fetchcolumn('SELECT groupid FROM ' . tablename('mc_groups') . ' WHERE uniacid = :uniacid AND isdefault = 1', array(':uniacid' => $this->uniacid));
            $user = array(
                'uniacid' => $this->uniacid,
                'email' => $dzMember['email'],
                'salt' => random(8),
                'groupid' => (int) $defaultGroupId,
                'createtime' => time(),
            );
            $user['password'] = md5($get['password'] . $user['salt'] . $_W['config']['setting']['authkey']);
            pdo_insert('mc_members', $user);
            $uid = pdo_insertid();
            pdo_insert('mc_mapping_ucenter', array('uniacid' => $this->uniacid, 'uid' => $uid, 'centeruid' => $ucenterUserId));
            // $openid = $this->discuzDb->query('SELECT uid FROM ' . $this->discuzTablePrefix . 'ucenter_wechat_members where uid = 1')->fetchColumn();
            $openid = null;
            pdo_update('mc_mapping_fans', array('uid' => $uid), array('uniacid' => $this->uniacid, 'acid' => $this->uniacid, 'openid' => $openid));
            $user['uid'] = $uid;

            return _mc_login($user) ? API_RETURN_SUCCEED : API_RETURN_FAILED;
        }
    }

    /**
     * 同步注销
     *
     * @global array $_G
     * @param array $get
     * @param array $post
     * @return mixed
     */
    function synlogout($get, $post)
    {
        global $_W;
        require IA_ROOT . '/app/common/bootstrap.app.inc.php';
        unset($_SESSION);
        session_destroy();
        isetcookie('logout', 1, 60);
        return API_RETURN_SUCCEED;
    }

    /**
     * 同步修改用户密码
     *
     * 需要修改 uc_server\control\admin\user.php 和 uc_server\control\user.php 中的 updatepw 通知代码，附加修改后的密码。
     *
     * @global array $_G
     * @param array $get
     * @param array $post
     * @return mixed
     */
    function updatepw($get, $post)
    {
        global $_W;

        if (!API_UPDATEPW) {
            return API_RETURN_FORBIDDEN;
        }

        // 未修改 UCenter Server 源码的情况下，password 是没值传递过来的
        if (!empty($get['password'])) {
            $user = pdo_fetch('SELECT uid, salt FROM ' . tablename('mc_members') . ' WHERE uniacid = :uniacid AND nickname = :nickname', array(':uniacid' => $this->uniacid, ':nickname' => $get['username']));
            if ($user) {
                return pdo_update('mc_members', array('password' => md5($get['password'] . $user['salt'] . $_W['config']['setting']['authkey'])), array('uid' => $user['uid'])) ? API_RETURN_SUCCEED : API_RETURN_FAILED;
            } else {
                return API_RETURN_FAILED;
            }
        } else {
            return API_RETURN_FAILED;
        }
    }

    function updatebadwords($get, $post)
    {
        global $_G;

        if (!API_UPDATEBADWORDS) {
            return API_RETURN_FORBIDDEN;
        }

        $data = array();
        if (is_array($post)) {
            foreach ($post as $k => $v) {
                $data['findpattern'][$k] = $v['findpattern'];
                $data['replace'][$k] = $v['replacement'];
            }
        }
        $cachefile = DISCUZ_ROOT . './uc_client/data/cache/badwords.php';
        $fp = fopen($cachefile, 'w');
        $s = "<?php\r\n";
        $s .= '$_CACHE[\'badwords\'] = ' . var_export($data, TRUE) . ";\r\n";
        fwrite($fp, $s);
        fclose($fp);

        return API_RETURN_SUCCEED;
    }

    function updatehosts($get, $post)
    {
        global $_G;

        if (!API_UPDATEHOSTS) {
            return API_RETURN_FORBIDDEN;
        }

        $cachefile = DISCUZ_ROOT . './uc_client/data/cache/hosts.php';
        $fp = fopen($cachefile, 'w');
        $s = "<?php\r\n";
        $s .= '$_CACHE[\'hosts\'] = ' . var_export($post, TRUE) . ";\r\n";
        fwrite($fp, $s);
        fclose($fp);

        return API_RETURN_SUCCEED;
    }

    /**
     * 更新应用配置
     *
     * @global array $_G
     * @param array $get
     * @param array $post
     * @return mixed
     */
    function updateapps($get, $post)
    {
        if ($post['UC_API']) {
            $post['UC_API'] = addslashes($post['UC_API']);
        }

        if (!API_UPDATEAPPS) {
            return API_RETURN_FORBIDDEN;
        }

        $UC_API = '';
        if ($post['UC_API']) {
            $UC_API = str_replace(array('\'', '"', '\\', "\0", "\n", "\r"), '', $post['UC_API']);
            unset($post['UC_API']);
        }

        $cachefile = IA_ROOT . './framework/library/uc/data/cache/apps.php';
        $fp = fopen($cachefile, 'w');
        $s = "<?php\r\n";
        $s .= '$_CACHE[\'apps\'] = ' . var_export($post, true) . ";\r\n";
        fwrite($fp, $s);
        fclose($fp);

        return API_RETURN_SUCCEED;
    }

    function updateclient($get, $post)
    {
        global $_G;

        if (!API_UPDATECLIENT) {
            return API_RETURN_FORBIDDEN;
        }

        $cachefile = DISCUZ_ROOT . './uc_client/data/cache/settings.php';
        $fp = fopen($cachefile, 'w');
        $s = "<?php\r\n";
        $s .= '$_CACHE[\'settings\'] = ' . var_export($post, TRUE) . ";\r\n";
        fwrite($fp, $s);
        fclose($fp);

        return API_RETURN_SUCCEED;
    }

    function updatecredit($get, $post)
    {
        global $_G;

        if (!API_UPDATECREDIT) {
            return API_RETURN_FORBIDDEN;
        }

        $credit = $get['credit'];
        $amount = $get['amount'];
        $uid = $get['uid'];
        if (!getuserbyuid($uid)) {
            return API_RETURN_SUCCEED;
        }

        updatemembercount($uid, array($credit => $amount));
        C::t('common_credit_log')->insert(array('uid' => $uid, 'operation' => 'ECU', 'relatedid' => $uid, 'dateline' => time(), 'extcredits' . $credit => $amount));

        return API_RETURN_SUCCEED;
    }

    function getcredit($get, $post)
    {
        global $_G;

        if (!API_GETCREDIT) {
            return API_RETURN_FORBIDDEN;
        }
        $uid = intval($get['uid']);
        $credit = intval($get['credit']);
        $_G['uid'] = $_G['member']['uid'] = $uid;
        return getuserprofile('extcredits' . $credit);
    }

    function getcreditsettings($get, $post)
    {
        global $_G;

        if (!API_GETCREDITSETTINGS) {
            return API_RETURN_FORBIDDEN;
        }

        $credits = array();
        foreach ($_G['setting']['extcredits'] as $id => $extcredits) {
            $credits[$id] = array(strip_tags($extcredits['title']), $extcredits['unit']);
        }

        return $this->_serialize($credits);
    }

    function updatecreditsettings($get, $post)
    {
        global $_G;

        if (!API_UPDATECREDITSETTINGS) {
            return API_RETURN_FORBIDDEN;
        }

        $outextcredits = array();
        foreach ($get['credit'] as $appid => $credititems) {
            if ($appid == UC_APPID) {
                foreach ($credititems as $value) {
                    $outextcredits[$value['appiddesc'] . '|' . $value['creditdesc']] = array(
                        'appiddesc' => $value['appiddesc'],
                        'creditdesc' => $value['creditdesc'],
                        'creditsrc' => $value['creditsrc'],
                        'title' => $value['title'],
                        'unit' => $value['unit'],
                        'ratiosrc' => $value['ratiosrc'],
                        'ratiodesc' => $value['ratiodesc'],
                        'ratio' => $value['ratio']
                    );
                }
            }
        }
        $tmp = array();
        foreach ($outextcredits as $value) {
            $key = $value['appiddesc'] . '|' . $value['creditdesc'];
            if (!isset($tmp[$key])) {
                $tmp[$key] = array('title' => $value['title'], 'unit' => $value['unit']);
            }
            $tmp[$key]['ratiosrc'][$value['creditsrc']] = $value['ratiosrc'];
            $tmp[$key]['ratiodesc'][$value['creditsrc']] = $value['ratiodesc'];
            $tmp[$key]['creditsrc'][$value['creditsrc']] = $value['ratio'];
        }
        $outextcredits = $tmp;

        $cachefile = DISCUZ_ROOT . './uc_client/data/cache/creditsettings.php';
        $fp = fopen($cachefile, 'w');
        $s = "<?php\r\n";
        $s .= '$_CACHE[\'creditsettings\'] = ' . var_export($outextcredits, TRUE) . ";\r\n";
        fwrite($fp, $s);
        fclose($fp);

        return API_RETURN_SUCCEED;
    }

    function addfeed($get, $post)
    {
        global $_G;

        if (!API_ADDFEED) {
            return API_RETURN_FORBIDDEN;
        }
        return API_RETURN_SUCCEED;
    }

}
