<?php

/**
 * 数据库操作类
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class DB
{

    private static $instance;
    public static $tablePrefix = '';

    private function __construct()
    {

    }

    private function __clone()
    {

    }

    final public static function table($tableName)
    {
        return self::$tablePrefix . $tableName;
    }

    public static function quoteName($name)
    {
        return "`{$name}`";
    }

    public static function quoteValue($value)
    {
        if (is_null($value)) {
            return 'NULL';
        } elseif (is_numeric($value)) {
            return $value;
        } else {
            return "'{$value}'";
        }
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8';"));
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return self::$instance;
    }

    final public static function __callStatic($chrMethod, $arrArguments)
    {
        $objInstance = self::getInstance();
        return call_user_func_array(array($objInstance, $chrMethod), $arrArguments);
    }

    /**
     * 简单 SQL 条件构造
     * 示例
     * 1. simpleBuildWhere('id = 1') 生成 id = 1
     * 2. simpleBuildWhere(array('id' => 1)) 生成 id = 1
     * 3. simpleBuildWhere(array('id' => array(1, 2)) 生成 id IN (1, 2)
     * 4. simpleBuildWhere(array('id' => array(1)) 生成 id = 1
     * 5. simpleBuildWhere(array('name' => null)) 生成 name is NULL
     *
     * @param string|array $condition
     * @param array $bindValues
     * @return string
     */
    public static function simpleBuildWhere($condition, $bindValues = array())
    {
        $whereString = '';
        if (!empty($condition)) {
            if (is_string($condition)) {
                $whereString = $condition;
            } elseif (is_array($condition)) {
                $where = array();
                foreach ($condition as $name => $cond) {
                    if (is_null($cond)) {
                        $where[] = self::quoteName($name) . ' IS NULL';
                    } elseif (!is_array($cond)) {
                        $where[] = self::quoteName($name) . ' = ' . self::quoteValue((string) $cond);
                    } else {
                        $cond = array_values($cond);
                        if (count($cond) == 1) {
                            $where[] = self::quoteName($name) . ' = ' . self::quoteValue($cond[0]);
                        } else {
                            $where[] = self::quoteName($name) . ' IN (' . implode(", ", array_map(function($v) {
                                        return self::quoteValue($v);
                                    }, $cond)) . ')';
                        }
                    }
                }
                $whereString = implode(' AND ', $where);
            }

            if ($bindValues) {
                foreach ($bindValues as $key => $value) {
                    $bindValues[$key] = self::quoteValue($value);
                }
                $whereString = strtr($whereString, $bindValues);
            }
        }

        return $whereString;
    }

    public static function _execute($sql, $bindValues = array())
    {
        $sth = self::prepare($sql);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        foreach ($bindValues as $key => $value) {
            $sth->bindValue($key, $value);
        }
        $sth->execute();

        return $sth;
    }

    final public static function one($sql, $bindValues = array())
    {
        return self::_execute($sql, $bindValues)->fetch();
    }

    final public static function column($sql, $bindValues = array())
    {
        $column = array();
        $sth = self::_execute($sql, $bindValues);
        while ($row = $sth->fetch()) {
            $column[] = reset($row);
        }

        return $column;
    }

    final public static function all($sql, $bindValues = array())
    {
        return self::_execute($sql, $bindValues)->fetchAll();
    }

    final public static function scalar($sql, $bindValues = array())
    {
        return self::_execute($sql, $bindValues)->fetchColumn();
    }

    final public static function exists($sql, $bindValues = array())
    {
        return self::scalar($sql, $bindValues) ? true : false;
    }

    final public static function count($sql, $bindValues = array())
    {
        $count = 0;
        $sth = self::_execute($sql, $bindValues);
        while ($row = $sth->fetch()) {
            $count = reset($row);
        }

        return $count;
    }

    /**
     * 插入数据
     * @param string $tableName
     * @param array $data
     * @param boolean $getLastInsertId
     * @return integer|boolean
     * @throws Exception
     */
    final public static function insert($tableName, $data, $getLastInsertId = false)
    {
        if (empty($data) || !is_array($data)) {
            return null;
        }

        $names = $values = array();
        foreach ($data as $name => $value) {
            $names[] = self::quoteName($name);
            $values[] = self::quoteValue($value);
        }
        try {
            $sql = "INSERT INTO " . self::table($tableName) . '(' . implode(', ', $names) . ') VALUES (' . implode(', ', $values) . ')';
            $sth = DB::prepare($sql);
            $res = $sth->execute();
            if ($getLastInsertId) {
                return DB::lastInsertId();
            } else {
                return $res ? true : false;
            }
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }

    /**
     * 数据批量插入
     * @param string $tableName
     * @param array $columns
     * @param array $rows
     * @return integer|null
     */
    final public static function batchInsert($tableName, $columns, $rows)
    {
        if (empty($tableName) || empty($columns) || empty($rows)) {
            return null;
        }

        $values = array();
        foreach ($rows as $row) {
            $values[] = '(' . implode(', ', array_map(function($value) {
                        return self::quoteValue($value);
                    }, $row)) . ')';
        }
        $sql = 'INSERT INTO ' . self::table($tableName) . '(' . implode(', ', array_map(function ($column) {
                    return self::quoteName($column);
                }, $columns)) . ') VALUES ' . implode(', ', $values);

        return self::_execute($sql)->rowCount();
    }

    final public static function update($tableName, $data, $condition)
    {
        if (empty($data) || !is_array($data)) {
            return null;
        }
        $sets = array();
        foreach ($data as $name => $value) {
            $sets[] = self::quoteName($name) . ' = ' . self::quoteValue($value);
        }
        $sql = 'UPDATE ' . self::table($tableName) . ' SET ' . implode(', ', $sets);
        if (!empty($condition)) {
            if (is_string($condition)) {
                $sql .= " WHERE {$condition}";
            } elseif (is_array($condition)) {
                $where = array();
                foreach ($condition as $key => $value) {
                    $where[] = self::quoteName($key) . ' = ' . self::quoteValue($value);
                }
                if ($where) {
                    $sql .= ' WHERE ' . implode(' AND ', $where);
                }
            }
        }
        try {
            $sth = DB::prepare($sql);
            $res = $sth->execute();
            return $res ? true : false;
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }

    /**
     * 根据指定条件删除表记录
     * @param string $tableName
     * @param string|array $condition
     * @param array $bindValues
     * @return integer
     */
    final public static function delete($tableName, $condition, $bindValues = array())
    {
        $where = self::simpleBuildWhere($condition, $bindValues);
        $sql = 'DELETE FROM ' . self::table($tableName) . ($where ? " WHERE {$where}" : '');

        return self::_execute($sql)->rowCount();
    }

    final public static function exec($sql, $bindValues)
    {
        $sth = DB::prepare($sql);
        foreach ($bindValues as $key => $value) {
            $sth->bindValue($key, $value);
        }

        $sth->execute();
    }

}
