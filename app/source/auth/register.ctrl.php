<?php
/**
 * [WeEngine System] Copyright (c) 2014 WE7.CC
 * WeEngine is NOT a free software, it under the license terms, visited http://www.we7.cc/ for more details.
 */
defined('IN_IA') or exit('Access Denied');
$openid = $_W['openid'];
$dos = array('register', 'uc','sendsms','mregister','sendsms1','forget');
$do = in_array($do, $dos) ? $do : 'register';

$setting = uni_setting($_W['uniacid'], array('uc', 'passport'));
$uc_setting = $setting['uc'] ? $setting['uc'] : array();
$item = $setting['passport']['item'] ? $setting['passport']['item'] : 'mobile';
$audit = @intval($setting['passport']['audit']);
$ltype = empty($setting['passport']['type']) ? 'hybird' : $setting['passport']['type'];
$rtype = trim($_GPC['type']) ? trim($_GPC['type']) : 'email';
$forward = url('mc');
defined('IN_IA') or exit('Access Denied');
define('IN_MOBILE', true);
define('UC_CONNECT', 'mysql');
define('UC_DBHOST', 'localhost');
define('UC_DBUSER', 'guest');
define('UC_DBPW', 'yemA7oM4N4fowek8Fi7abez66OBe2i');
define('UC_DBNAME', 'bbs_tangbb_com');
define('UC_DBCHARSET', 'utf8');
define('UC_DBTABLEPRE', '`bbs_tangbb_com`.tbb_ucenter_');
define('UC_DBCONNECT', '0');
define('UC_KEY', '0727YklWa8RCTZMgYYUz9D2BnjA139bQGTDpBh0xCYQaZRzwBa0LyS4DiSrkMXbw+bzDG8qi6cDZSfbv9Nuf2z7svhg');
define('UC_API', 'http://bbs.tangbb.com/uc_server');
define('UC_CHARSET', 'utf-8');
define('UC_IP', '');
define('UC_APPID', '3');
define('UC_PPP', '20');
require '../framework/library/uc/client.php';

if(!empty($_GPC['forward'])) {
	$forward = './index.php?' . base64_decode($_GPC['forward']) . '#wechat_redirect';
}
if(!empty($_W['member']) && (!empty($_W['member']['mobile']) || !empty($_W['member']['email']))) {
	header('location: ' . $forward);
	exit;
}

if($do == 'register') {
	if($_W['ispost'] && $_W['isajax']) {
		$sql = 'SELECT `uid` FROM ' . tablename('mc_members') . ' WHERE `uniacid`=:uniacid';
		$pars = array();
		$pars[':uniacid'] = $_W['uniacid'];
		$code = trim($_GPC['code']);
		$username = trim($_GPC['username']);
		$password = trim($_GPC['password']);
		if (empty($code)) {
			$repassword = trim($_GPC['repassword']);
			if ($repassword != $password) {
				message('密码输入不一致', referer(), 'error');
			}
						if($item == 'email') {
				if(preg_match(REGULAR_EMAIL, $username)) {
					$type = 'email';
					$sql .= ' AND `email`=:email';
					$pars[':email'] = $username;
                                        $email=$username;
				} else {
					message('邮箱格式不正确', referer(), 'error');
				}
			} elseif($item == 'mobile') {
				if(preg_match(REGULAR_MOBILE, $username)) {
					$type = 'mobile';
					$sql .= ' AND `mobile`=:mobile';
					$pars[':mobile'] = $username;
				} else {
					message('手机号格式不正确', referer(), 'error');
				}
			} else {
				if (preg_match(REGULAR_MOBILE, $username)) {
					$type = 'mobile';
					$sql .= ' AND `mobile`=:mobile';
					$pars[':mobile'] = $username;
				} elseif (preg_match(REGULAR_EMAIL, $username)) {
					$type = 'email';
					$sql .= ' AND `email`=:email';
					$pars[':email'] = $username;
				} else {
					message('用户名格式错误', referer(), 'error');
				}
			}
		} else {
			load()->model('utility');
			if (!code_verify($_W['uniacid'], $username, $password)) {
				message('验证码错误', referer(), 'error');
			} else {
				pdo_delete('uni_verifycode', array('receiver' => $username));
			}
			if (preg_match(REGULAR_MOBILE, $username)) {
				$type = 'mobile';
				$sql .= ' AND `mobile`=:mobile';
				$pars[':mobile'] = $username;
			} else {
				message('用户名格式错误', referer(), 'error');
			}
                        
                        
                  
			if ($ltype != 'code' && $audit == '1') {
				$audit_password = trim($_GPC['audit_password']);
				$audit_repassword = trim($_GPC['audit_repassword']);
				if ($audit_password != $audit_repassword) {
					message('密码输入不一致', referer(), 'error');
				}
				$password = $audit_password;
			}
			if ($ltype == 'code' && $audit == '1') {
				$password = '';
			}
		}
		$user = pdo_fetch($sql, $pars);
		if(!empty($user)) {
			message('该用户名已被注册', referer(), 'error');
		}
		    if(!empty($_W['openid'])) {
			$fan = mc_fansinfo($_W['openid']);
			if (!empty($fan)) {
				$map_fans = $fan['tag'];
			}
			if (empty($map_fans) && isset($_SESSION['userinfo'])) {
				$map_fans = unserialize(base64_decode($_SESSION['userinfo']));
			}
		}

		$default_groupid = pdo_fetchcolumn('SELECT groupid FROM ' .tablename('mc_groups') . ' WHERE uniacid = :uniacid AND isdefault = 1', array(':uniacid' => $_W['uniacid']));
		$data = array(
			'uniacid' => $_W['uniacid'], 
			'salt' => random(8),
			'groupid' => $default_groupid, 
			'createtime' => TIMESTAMP,
		);
		
		$data['email'] = $type == 'email'  ? $username : '';
		$data['mobile'] = $type == 'mobile' ? $username : '';
		if (!empty($password)) {
			$data['password'] = md5($password . $data['salt'] . $_W['config']['setting']['authkey']);
		}
		if (empty($type)) {
			$data['mobile'] = $username;
		}
		if(!empty($map_fans)) {
			$data['nickname'] = $map_fans['nickname'];
			$data['gender'] = $map_fans['sex'];
			$data['residecity'] = $map_fans['city'] ? $map_fans['city'] . '市' : '';
			$data['resideprovince'] = $map_fans['province'] ? $map_fans['province'] . '省' : '';
			$data['nationality'] = $map_fans['country'];
			$data['avatar'] = rtrim($map_fans['headimgurl'], '0') . 132;
		}
		
                if (empty($username) || (!empty($username) && (uc_user_checkname($username) < 0 || uc_user_checkemail($email) < 0))) {
                          message('用户名格式错误谢谢', referer(), 'error');
                 }
                 
		pdo_insert('mc_members', $data);
                
		$user['uid'] = pdo_insertid();
                
                if(empty($email)){
                 $email = uniqid('wx') . '@null.com';
                }
                
                $uids = uc_user_register($username, $password, $email);
      
                if (!empty($uids)) {
                    
			 pdo_insert('mc_mapping_ucenter', array('uniacid' => 1, 'uid' => $user['uid'], 'centeruid' => $uids));
                         
		}
               
                list($uid, $username, $password, $email) = uc_user_login($username, $password);
       
                if ($uid > 0) {
                           
                   echo $ucsynlogin = uc_user_synlogin($uid);
                   
                }
  
		if (!empty($fan) && !empty($fan['fanid'])) {
			pdo_update('mc_mapping_fans', array('uid'=>$user['uid']), array('fanid'=>$fan['fanid']));
		}
		if(_mc_login($user)) {
                    
			message('注册成功！', referer(), 'success');
		}
		message('未知错误导致注册失败', referer(), 'error');
	}
	template('auth/register');
	exit;
}

  if($do == 'sendsms') {
        
            if(time()-$_SESSION['code_time']<60){   
                message('发送短信间隔太短！', referer(), 'error');
                exit();
            }
            $sql = 'SELECT `uid` FROM ' . tablename('mc_members') . ' WHERE `uniacid`=:uniacid';
            $pars = array();
	    $pars[':uniacid'] = $_W['uniacid'];
            $username = trim($_GPC['mobile_phone']);
         
             if(preg_match(REGULAR_MOBILE, $username)) {
					$type = 'mobile';
					$sql .= ' AND `mobile`=:mobile';
					$pars[':mobile'] = $username;
                 } else {
		    message('手机号格式不正确', referer(), 'error');
                }
            
            $user = pdo_fetch($sql, $pars);
            
            if(!empty($user)) {
			message('该用户名已被注册', referer(), 'error');
                }else{
                    
                 $code=rand(100000,999999);   
                
                $_SESSION['code']=$code;
                
                $_SESSION['mobile_phone']=$username;
                
               
      
                $url='http://61.147.98.117:9001';//系统接口地址
                $content=urlencode(iconv("UTF-8", "GB2312//IGNORE", "您的验证码是:".$code.",5分钟后过期，请您及时验证!"));
                $username="13975164016";//用户名
                $password="MTIz";//密码百度BASE64加密后密文
                $mobile=$_SESSION['mobile_phone'];
                     
                if(time()-$_SESSION['code_time']<60){   
                message('发送短信间隔太短！', referer(), 'error');
                exit();
             }
                
                
                $url=$url."/servlet/UserServiceAPI?method=sendSMS&extenno=&isLongSms=0&username=".$username."&password=".$password."&smstype=1&mobile=".$mobile."&content=".$content;
                $html = file_get_contents($url);
                $_SESSION['code_time']=time();
                message('短信发送成功！', referer(), 'success');
              
                }
            exit();
 }
    if($do == 'sendsms1') {
        
            if(time()-$_SESSION['code_time']<60){
                message('发送短信间隔太短！', referer(), 'error');
                exit();
                
            }
            $sql = 'SELECT `uid` FROM ' . tablename('mc_members') . ' WHERE `uniacid`=:uniacid';
            $pars = array();
	    $pars[':uniacid'] = $_W['uniacid'];
            $username = trim($_GPC['mobile_phone']);
         
             if(preg_match(REGULAR_MOBILE, $username)) {
					$type = 'mobile';
					$sql .= ' AND `mobile`=:mobile';
					$pars[':mobile'] = $username;
                 } else {
		    message('手机号格式不正确12', referer(), 'error');
                }
            
            $user = pdo_fetch($sql, $pars);
            
            if(empty($user)) {
			message('该用户名不存在请重新输入', referer(), 'error');
                }else{
                    
                $code=rand(100000,999999);   
                
                $_SESSION['code']=$code;
                
                $_SESSION['mobile_phone']=$username;
                
            
      
                $url='http://61.147.98.117:9001';//系统接口地址
                $content=urlencode(iconv("UTF-8", "GB2312//IGNORE", "您的验证码是:".$code.",5分钟后过期，请您及时验证!"));
                $username="13975164016";//用户名
                $password="MTIz";//密码百度BASE64加密后密文
                $mobile=$_SESSION['mobile_phone'];
                     
               if(time()-$_SESSION['code_time']<60){   
                message('发送短信间隔太短！', referer(), 'error');
                exit();
             }
                
                $url=$url."/servlet/UserServiceAPI?method=sendSMS&extenno=&isLongSms=0&username=".$username."&password=".$password."&smstype=1&mobile=".$mobile."&content=".$content;
                $html = file_get_contents($url);
                $_SESSION['code_time']=time();
                message('短信发送成功！', referer(), 'success');
              
                }
            exit();
 }
 
 
  if( $do == 'forget') {
		$sql = 'SELECT `uid`,`salt`,`password`  FROM ' . tablename('mc_members') . ' WHERE `uniacid`=:uniacid';
		$pars = array();
		$pars[':uniacid'] = $_W['uniacid'];
		$code = trim($_GPC['reg_code']);
		$username = trim($_GPC['reg_tel']);
		$password = trim($_GPC['reg_psd']);
                
       
                if( $_SESSION['code']!=$code or  $_SESSION['mobile_phone']!=$username ){
                    message('验证信息不正确', referer(), 'error'); 
                   }
               

			$repassword = trim($_GPC['repassword']);
                        if(preg_match(REGULAR_MOBILE, $username)) {
					$type = 'mobile';
					$sql .= ' AND `mobile`=:mobile';
					$pars[':mobile'] = $username;
			} else {
				message($username, referer(), 'error');
			}

		$user = pdo_fetch($sql, $pars);
      
		if(empty($user)) {
			message('该用户名不存在请重新输入', referer(), 'error');
		} 
                
		$data['password'] = md5($password .$user['salt']);
	  
                pdo_update('mc_members', $data, array('uid'=>$user['uid']));

                message('密码修改成功', referer(), 'success');
     
                exit();
	
     }

     
     if( $do == 'mregister') {
		$sql = 'SELECT `uid` FROM ' . tablename('mc_members') . ' WHERE `uniacid`=:uniacid';
		$pars = array();
		$pars[':uniacid'] = $_W['uniacid'];
		$code = trim($_GPC['reg_code']);
		$username = trim($_GPC['reg_tel']);
		$password = trim($_GPC['reg_psd']);
                
       
                if( $_SESSION['code']!=$code or  $_SESSION['mobile_phone']!=$username ){
                    message('验证信息不正确', referer(), 'error'); 
                   }
               

			$repassword = trim($_GPC['repassword']);
                        if(preg_match(REGULAR_MOBILE, $username)) {
					$type = 'mobile';
					$sql .= ' AND `mobile`=:mobile';
					$pars[':mobile'] = $username;
			} else {
				message('手机号格式不正确', referer(), 'error');
			}

  
		$user = pdo_fetch($sql, $pars);
      
		if(!empty($user)) {
			message('该用户名已被注册', referer(), 'error');
		} 
       
		$default_groupid = pdo_fetchcolumn('SELECT groupid FROM ' .tablename('mc_groups') . ' WHERE uniacid = :uniacid AND isdefault = 1', array(':uniacid' => $_W['uniacid']));
		$data = array(
			'uniacid' => $_W['uniacid'], 
			'salt' => random(8),
			'groupid' => $default_groupid, 
			'createtime' => TIMESTAMP,
		);

		$data['mobile'] = $type == 'mobile' ? $username : '';
		if (!empty($password)) {
			$data['password'] = md5($password . $data['salt']);
		}
		if (empty($type)) {
			$data['mobile'] = $username;
		}
 
		pdo_insert('mc_members', $data);

		$user['uid'] = pdo_insertid();

		if(_mc_login($user)) {
                    
			message('注册成功！', referer(), 'success');
		}
                
		message('未知错误导致注册失败', referer(), 'error');
                exit();
	
     }
    

