<?php

/**
 * 任务处理类
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
trait WorkerTrait
{

    /**
     * 发送任务请求
     * @param array $postData
     * @return mixed null 请求失败, true 成功 array 失败，返回错误信息
     */
    protected static function _send($postData)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://bbs.tangbangbang.cn/api/dz/index.php/tasks');
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        $content = curl_exec($curl);
        $status = curl_getinfo($curl);
        curl_close($curl);

        if (isset($status['http_code']) && $status['http_code'] == 200) {
            $res = json_decode($content, true);
            if ($res !== null && $res['success']) {
                return true;
            } else {
                return false;
            }
        } else {
            $res = $content;
        }

        return $res;
    }

}

interface WorkerInterface
{

    public static function send($body);
}

abstract class WorkerAbstract
{

    use WorkerTrait;
}

/**
 * 微信客服消息发送
 */
class WechatStaffWorker extends WorkerAbstract
{

    public static function send($body)
    {
        return self::_send(array('type' => 'wechat.staff', 'body' => serialize($body)));
    }

}

/**
 * 微信模版消息发送
 */
class WechatNoticeWorker extends WorkerAbstract
{

    public static function send($body)
    {
        if (isset($body['url'])) {
            if (stripos($body['url'], 'http://') === false) {
                $body['url'] = 'http://bbs.tangbangbang.cn/' . $body['url'];
            }
            $body['url'] = "http://passport.tangbangbang.cn/redirect?toUrl=" . rtrim(strtr(base64_encode($body['url']), '+/', '-_'), '=');
        }
        return self::_send(array('type' => 'wechat.notice', 'body' => serialize($body)));
    }

}

/**
 * 微信图像处理
 */
class WechatAvatarWorker extends WorkerAbstract
{

    public static function send($body)
    {
        return self::_send(array('type' => 'wechat.avatar', 'body' => serialize($body)));
    }

}

/**
 * 微信图片发送
 */
class WechatImageWorker extends WorkerAbstract
{

    public static function send($body)
    {
        return self::_send(array('type' => 'wechat.image', 'body' => serialize($body)));
    }

}
