$(document).ready(function(){
    var storage = window.localStorage,
        a1 = storage["tab_name"],
        b1 = storage["tab_sex"], 
        c1 = storage["tab_phone"],
        d1 = storage["card_no"],
        j1 = storage["user_area_num"], 
        e1 = storage["user_area"],
        f1 = storage["user_manifesto"], 
        i1 = storage["video"], 
        s1 = storage["s"];
    　  if(s1=="yes"){
        $(".tab_name").val(a1);
        if(b1=="0"){
            $("#tab_sex1").attr("checked", true);
        }else{
            $("#tab_sex2").attr("checked", true);
        }
        $(".tab_phone").val(c1);
        $(".card_no").val(d1);
        $(".user_area").val(e1);
        $(".user_area_num").val(j1);
        $(".user_manifesto").val(f1);
        $(".video").val(i1);  
    }
});
$("input,textarea").blur(function(){
    var a = $(".tab_name").val(),
        c = $(".tab_phone").val(),
        d = $(".card_no").val(),
        j = $(".user_area_num").val(),
        e = $(".user_area").val(),
        f = $(".user_manifesto").val(),
        i = $(".video").val();
        if($("input[name='sex']").is(':checked')) {
            b = $(this).val();
        }
    localStorage.setItem("tab_name", a);
    localStorage.setItem("tab_sex", b);
    localStorage.setItem("tab_phone", c);
    localStorage.setItem("card_no", d);
    localStorage.setItem("user_area", e);
    localStorage.setItem("user_manifesto", f);
    localStorage.setItem("user_area_num", j);
    localStorage.setItem("video", i);
    localStorage.setItem("s", "yes");
});
function postcheck() {

    if (document.wfform.name.value == "") {
        alert('请填写姓名！');
        document.wfform.name.focus();
        return false;
    }
    if (document.wfform.phone.value == "") {
        alert('请填写手机号码！');
        document.wfform.phone.focus();
        return false;
    }
    var reg1 = /^[a-zA-Z\u4e00-\u9fa5][a-zA-Z0-9\u4e00-\u9fa5]*$/;
    if (!reg1.test(document.wfform.name.value.replace(/ /g,''))) {
        alert('姓名格式不正确，只支持中文，英文，数字，不能以数字开头');
        document.wfform.name.focus();
        return false;
    }
    
    var reg2 = /^1[3,4,5,7,8]\d{9}$/;
    if (!reg2.test(document.wfform.phone.value)) {
        alert('手机号码格式不正确，请填写正确的手机号码！');
        document.wfform.phone.focus();
        return false;
    }
    if (document.wfform.card_no.value == "") {
    	alert('请填写身份证号码');
    	document.wfform.card_no.focus();
        return false;
    };
    var reg3 = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;  
    if(!reg3.test(document.wfform.card_no.value)){
    	alert('身份证输入不合法，请填写正确的身份证号码！');
        document.wfform.card_no.focus();
        return false;
    };
    if (document.wfform.user_area.value == "") {
    	alert('请选择赛区！');
    	document.wfform.user_area.focus();
        return false;
    };
     if (document.wfform.user_manifesto.value == "") {
        alert('请填写你的参赛宣言！');
        document.wfform.user_manifesto.focus();
        return false;
    };
    if (document.wfform.img1.value == "") {
    	alert('请上传本人手持身份证照片！');
    	document.wfform.img1.focus();
        return false;
    };
     if (document.wfform.img3.value == "") {
        alert('请上传本人面部清晰的生活照！');
        document.wfform.img3.focus();
        return false;
    };
     if (document.wfform.video.value == "") {
        alert('请上传本人唱歌视频网址！');
        document.wfform.video.focus();
        return false;
    };
    var strRegex = "^((https|http|ftp|rtsp|mms)?://)"
        + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //ftp的user@
        + "(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184
        + "|" // 允许IP和DOMAIN（域名）
        + "([0-9a-z_!~*'()-]+\.)*" // 域名- www.
        + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." // 二级域名
        + "[a-z]{2,6})" // first level domain- .com or .museum
        + "(:[0-9]{1,4})?" // 端口- :80
        + "((/?)|" // a slash isn't required if there is no file name
        + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
    var reg4=new RegExp(strRegex);
    if(!reg4.test(document.wfform.video.value)){
        alert('请正确的网址！');
        document.wfform.video.focus();
        return false;
    }
    var $isChecked = $("#tab_deal").is(":checked");
    if($isChecked==false){
        alert('你还没同意2017香港青年音乐节协议');
    }
    else postData();
    return false;
}

function protocol_cont() {

    var t = ['<div class="protocol_cont">',
'               <img src="http://wx.tangbb.com/addons/xiaof_toupiao/template/mobile/apdMusic/images/close.png" alt="" class="close">',
'               <div class="protocol_box">',
'                   <h2>参赛协议及注意事项</h2>',
'                   <p>1、参赛者必须认真阅读《参赛协议及注意事项》，提供资料中不包含任何非法的或不适于发布的事项； </p>',
'                   <p>2、参赛者在专题页面在线报名； </p>',
'                   <p>3、参赛选手提供详尽、准确的参赛者资料，及时对资料做出必要的更新，使之符合详尽、准确的要求。如果提供的资料不正确，组委会有权取消您的参赛资格；  </p>',
'                   <p>4、由组委会组织拍摄的照片和影像版权归组委会所有； </p>',
'                   <p>5、参赛者承认组委会有权发布或公开参赛选手的必要资料和将该资料或其中包含的观念应用于大赛或大赛的宣传工作中而不负任何责任和义务；  </p>',
'                   <p>6、参赛者同意不就所提供的资料对组委会进行任何追索，并保证在任何第三方就参赛者提供的资料对组委会进行权利要求追索时，参赛者将承担责任。除非评选需要，组委会将不对您所提供的资料作进一步的审查，并且只有参赛者对自己所提供的资料的知识产权及合法性负全部责任； </p>',
'                   <p>7、参赛者要严格遵守大赛组委会制订的比赛规则；  </p>',
'                   <p>8、参赛者中有与任何经纪人公司，制作公司，媒体公司签有任何合约的，参赛前预先告知大赛主办方。 </p>',
'                   <p>9、组委会将负责制订比赛的程序和规则；决定比赛的时间、地点；设计比赛的内容和形式。并可以根据实际情况对上述各事项做出相应的合理变更。本条所述各项均将在比赛进行前公布，参赛者须遵守组委会的安排；  </p>',
'                   <p>10、参赛者不能采取任何不正当行为干预比赛进程，操纵或影响比赛结果；  </p>',
'                   <p>11、参赛者不能影响或阻碍其他参赛者的参赛内容和行为，不对其他参赛者采取任何不包括在合法竞赛范围内的行为；  </p>',
'                   <p>12、参赛者不得滥用比赛规则所赋予的参赛者权利。组委会有权在有足够证据证明参赛者做出本条款声明禁止的行为时，做出独立判断，取消参赛者的参赛资格；   </p>',
'                   <p>13、组委会将对比赛的合法举办以及公开性、公平性、公正性做出保证，组委会对比赛规则和评选规则拥有最终解释权。参赛者在对比赛结果存有异议时，应向组委会呈报，并由组委会根据比赛规则和评选规则做出最后裁定；   </p>',
'                   <p>14、组委会将根据比赛举办的实际情况决定获胜参赛者可以获得的荣誉和奖励，并将公开颁发所获荣誉和奖励。获奖者在获得荣誉和奖励的同时，有义务作为提供奖品赞助单位的形象代言人宣传奖品赞助单位的产品及形象。参加比赛的选手必须参加大赛主办单位组织的公益性及非公益性的活动； </p>',
'                   <p>15、组委会(分赛区)如遇人力不可抗拒因素的实际情况可延期或中止大赛，并且无需对参赛者负责。  </p>',
'                   <h3>参赛者本人完全理解并同意以上条款  （不以任何理由撤销或反悔）</h3>',
'                   <a href="#" class="in_back">返回报名</a>',
'               </div>',
'           </div>'].join("");

    $('body').append($(t));
    $('.bodyCover').show();
    $(".protocol_cont a").one('click', function() {
        $(".protocol_cont").remove();
        $('.bodyCover').hide();
    });
    $(".bodyCover,.close").on('click', function() {
        $(".protocol_cont").remove();
        $('.bodyCover').hide();
    })
}